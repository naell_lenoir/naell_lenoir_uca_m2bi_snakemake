# naell_lenoir_uca_m2bi_snakemake



## Snakemake Project

GitHub repository of Snakemake project 2023-2024.  
This project is based on the analysis of a RRBS data subset from "STATegra, a comprehensive multi-omics dataset of B-cell differentiation in mouse", David Gomez-Cabrero et al. 2019, https://doi.org/10.1038/s41597-019-0202-7.

## RRBS analysis

The snakemake workflows are located in the workflows/ directory.  
All the required environments are available in workflows/envs/. 
Config file is located in config/. 
You'll also need to have access to the RRBS subset from STATegra and to the reference genome of Mus musculus (GRCm39/mm10). Paths towards these data are symbolic-linked in data/reference/ and data/subset/ directories.  
A git clone is highly advised since every workflow is launchable from naell_lenoir_uca_m2bi_snakemake/.   
Now, everything should be fine !  


## Directories
logs/ directory is empty for now. Be aware that a logs/ directory also needs to be created before running the Methylkit analysis.  
results/ and other output directories will be automatically created.    
The starting tree architecture is shown below :    

```
.
|-- README.md
|-- config
|   `-- config.yml
|-- data
|   |-- reference -> /home/users/shared/data/Mus_musculus/current/fasta/
|   `-- subset -> /home/users/shared/data/stategra/rrbs/
|-- logs
`-- workflows
    |-- alignment.smk
    |-- envs
    |   |-- rrbs_bismark.yml
    |   |-- rrbs_methylkit.yml
    |   |-- rrbs_qc.yml
    |   `-- rrbs_trim.yml
    |-- genome_preparation.smk
    |-- methylkit-analysis.smk
    |-- qc-init.smk
    |-- qc-post.smk
    |-- rrbs_methylkit.R
    |-- sort-alignment.smk
    `-- trimming.smk
```

## Usage

To collect scripts, use the git command line client:  

Exemple with HTTPS protocol:  
git clone https://gitlab.com/naell_lenoir/naell_lenoir_uca_m2bi_snakemake.git  
This will create a naell_lenoir_uca_m2bi_snakemake repository in your home.  


Uploading your environment in HPC-formation cluster  
$ module load conda/4.12.0  
$ conda activate  
$ module load gcc/8.1.0 python/3.7.1 snakemake/7.15.1  

Due to some conflicts, you need to upload the environment using these exact commands, otherwise you won't be able to use snakemake.  


From master node, launching a job is looking like:  
[hpcloging~]$ snakemake --snakefile workflow/Snakefile.smk --jobs 1 --use-conda --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task 8 --time={resources.time}" [--other_options]
