configfile: "config/config.yml"
config=config["samples"]

rule all:
    input:
        expand("BismarkResults/{sample}_1_val_1_bismark_bt2_pe.bam", sample=config),
        expand("BismarkResults/{sample}_1_val_1_bismark_bt2_PE_report.txt", sample=config)

rule bismark_alignment:
    input:
        r1="results/trimming_rrbs/{sample}_1_val_1.fq.gz",
        r2="results/trimming_rrbs/{sample}_2_val_2.fq.gz"
    output:
        "BismarkResults/{sample}_1_val_1_bismark_bt2_pe.bam",
        "BismarkResults/{sample}_1_val_1_bismark_bt2_PE_report.txt"
    conda:
        "envs/rrbs_bismark.yml"

    resources:
        mem_mb=5000,
        time="10:00:00"

    shell : "bismark data/reference/ -1 {input.r1} -2 {input.r2} -o BismarkResults"


