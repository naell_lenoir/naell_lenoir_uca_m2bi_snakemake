configfile: "config/config.yml"

rule all:
    input:
        directory("data/reference/Bisulfite_Genome")

rule genome_preparation:
    input: 
        genref="data/reference/"
    output: 
        directory("data/reference/Bisulfite_Genome")

    conda:
        "envs/rrbs_bismark.yml"

    resources:
        mem_mb=5000,
        time="06:00:00"

    shell : "bismark_genome_preparation {input.genref}"
