configfile: "config/config.yml"
config=config["sorted"]

rule all:
    input:
        expand("Methylkit_analysis/{sample}_CpG.txt", sample=config),
        expand("Methylkit_analysis/{sample}_CpG_conversionStats.txt", sample=config),
        "Methylkit_analysis/hyper.DMR.bedGraph",
        "Methylkit_analysis/hypo.DMR.bedGraph"

rule methylkit:
    input:
        c0_r1="sscontrol_0h_B1_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        c0_r2="sscontrol_0h_B2_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        c0_r3="sscontrol_0h_B4_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        i0_r1="ssikaros_0h_B1_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        i0_r2="ssikaros_0h_B2_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        i0_r3="ssikaros_0h_B4_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        c24_r1="sscontrol_24h_B1_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        c24_r2="sscontrol_24h_B3_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        c24_r3="sscontrol_24h_B4_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        i24_r1="ssikaros_24h_B1_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        i24_r2="ssikaros_24h_B3_R1_1_val_1_bismark_bt2_pe_sorted.bam",
        i24_r3="ssikaros_24h_B4_R1_1_val_1_bismark_bt2_pe_sorted.bam"
    output:
        expand("Methylkit_analysis/{sample}_CpG.txt", sample=config),
        expand("Methylkit_analysis/{sample}_CpG_conversionStats.txt", sample=config),
        "Methylkit_analysis/hyper.DMR.bedGraph",
        "Methylkit_analysis/hypo.DMR.bedGraph"
    conda:
        "envs/rrbs_methylkit.yml"

    resources:
        mem_mb=5000,
        time="05:00:00"

    shell:
        """
        cd BismarkResults/sorted/
        Rscript ../../workflows/rrbs_methylkit.R {input.c0_r1} {input.c0_r2} {input.c0_r3} {input.i0_r1} {input.i0_r2} {input.i0_r3} &
        Rscript ../../workflows/rrbs_methylkit.R {input.c24_r1} {input.c24_r2} {input.c24_r3} {input.i24_r1} {input.i24_r2} {input.i24_r3} &
        """



