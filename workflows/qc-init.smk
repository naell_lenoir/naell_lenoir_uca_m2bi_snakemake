configfile: "config/config.yml"
config=config["samples"]

rule all: 
    input:
        expand("results/FastQC/{sample}_1_fastqc.zip", sample=config),
        expand("results/FastQC/{sample}_2_fastqc.zip", sample=config),
        expand("results/FastQC/{sample}_1_fastqc.html", sample=config),
        expand("results/FastQC/{sample}_2_fastqc.html", sample=config)

rule fastqc:
    input:
        expand("data/subset/{sample}_1.fastq.gz", sample=config),
        expand("data/subset/{sample}_2.fastq.gz", sample=config)
    output:
        expand("results/FastQC/{sample}_1_fastqc.zip", sample=config),
        expand("results/FastQC/{sample}_2_fastqc.zip", sample=config),
        expand("results/FastQC/{sample}_1_fastqc.html", sample=config),
        expand("results/FastQC/{sample}_2_fastqc.html", sample=config)
    
    conda: 
        "envs/rrbs_qc.yml"

    resources:
        mem_mb=5000,
        time="00:30:00"
    shell: "fastqc --outdir results/FastQC/ {input}"
