configfile: "config/config.yml"
config=config["samples"]

rule all: 
    input:
        expand("results/FastQC/rrbs_trimmed/{sample}_1_val_1_fastqc.zip", sample=config),
        expand("results/FastQC/rrbs_trimmed/{sample}_2_val_2_fastqc.zip", sample=config),
        expand("results/FastQC/rrbs_trimmed/{sample}_1_val_1_fastqc.html", sample=config),
        expand("results/FastQC/rrbs_trimmed/{sample}_2_val_2_fastqc.html", sample=config)

rule fastqc: 
    input:
        expand("results/trimming_rrbs/{sample}_1_val_1.fq.gz", sample=config),
        expand("results/trimming_rrbs/{sample}_2_val_2.fq.gz", sample=config)  
    output:
        expand("results/FastQC/rrbs_trimmed/{sample}_1_val_1_fastqc.zip", sample=config),
        expand("results/FastQC/rrbs_trimmed/{sample}_2_val_2_fastqc.zip", sample=config),
        expand("results/FastQC/rrbs_trimmed/{sample}_1_val_1_fastqc.html", sample=config),
        expand("results/FastQC/rrbs_trimmed/{sample}_2_val_2_fastqc.html", sample=config)

    conda:
        "envs/rrbs_qc.yml"
	
    resources:
        mem_mb=5000, 
        time="00:30:00"

    shell : "fastqc --outdir results/FastQC/rrbs_trimmed {input}"
