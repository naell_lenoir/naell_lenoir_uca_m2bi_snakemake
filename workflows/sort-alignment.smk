configfile: "config/config.yml"
config=config["samples"]

rule all:
    input:
        expand("BismarkResults/sorted/{sample}_1_val_1_bismark_bt2_pe_sorted.bam", sample=config)

rule sort_alignment:
    input:
        "BismarkResults/{sample}_1_val_1_bismark_bt2_pe.bam"
    output:
        "BismarkResults/sorted/{sample}_1_val_1_bismark_bt2_pe_sorted.bam"
        
    conda:
        "envs/rrbs_bismark.yml"

    resources:
        mem_mb=5000,
        time="00:30:00"

    shell : "samtools sort {input} > {output}"


