configfile: "config/config.yml"
config=config["samples"]

rule all: 
    input:
        expand("results/trimming_rrbs/{sample}_1.fastq.gz_trimming_report.txt", sample=config),
        expand("results/trimming_rrbs/{sample}_1_val_1.fq.gz", sample=config),
        expand("results/trimming_rrbs/{sample}_2.fastq.gz_trimming_report.txt", sample=config),
        expand("results/trimming_rrbs/{sample}_2_val_2.fq.gz", sample=config)

rule trimming: 
    input:
        r1="data/subset/{sample}_1.fastq.gz",
        r2="data/subset/{sample}_2.fastq.gz"
    output:
        "results/trimming_rrbs/{sample}_1.fastq.gz_trimming_report.txt",
        "results/trimming_rrbs/{sample}_1_val_1.fq.gz",
        "results/trimming_rrbs/{sample}_2.fastq.gz_trimming_report.txt",
        "results/trimming_rrbs/{sample}_2_val_2.fq.gz"

    conda:
        "envs/rrbs_trim.yml"
	
    resources:
        mem_mb=5000, 
        time="01:00:00"

    shell : "trim_galore --rrbs --paired -q 30 --clip_R1 5 --clip_R2 5 --three_prime_clip_R1 3 --three_prime_clip_R2 3 {input.r1} {input.r2} -o results/trimming_rrbs/"
